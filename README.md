# Minesweeper

Required:  Unity-2017.3.1f1 or higher

# How to start project: 

Open `_Scenes/GameScene` and Run Project.

# Tested

Tested on PC and Android.

## Links

+ [Android](https://goo.gl/5ctTw5) - *Get the APK*

## Description

+ Game logic start with GameController.cs
+ You can change Game Difficulty in BoardController.cs or in 'Game Popup'.
+ You can Load or Save Game by pressing Menu Button and choose action.
+ After game over you can see 'Result Popup'.


# Thank you for your time.

# Have a nice day !



