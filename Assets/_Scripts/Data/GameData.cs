﻿using System;

public class GameData
{
    private static BoardData _beginner = new BoardData {Rows = 10, Colls = 10, Mines = 10};
    private static BoardData _advanced = new BoardData {Rows = 20, Colls = 20, Mines = 40};
    private static BoardData _expert = new BoardData {Rows = 30, Colls = 30, Mines = 90};

    public static BoardData GetBoardData(GameDifficulty gameDifficulty)
    {
        if (gameDifficulty == GameDifficulty.Beginner)
        {
            return _beginner;
        }

        if (gameDifficulty == GameDifficulty.Advanced)
        {
            return _advanced;
        }

        if (gameDifficulty == GameDifficulty.Expert)
        {
            return _expert;
        }

        throw new Exception("There is no such BoardData for GameDifficulty in GameData: " + gameDifficulty);
    }

    public static GameDifficulty GetGameDifficultyByRows(int rows)
    {
        switch (rows)
        {
            case 10:
                return GameDifficulty.Beginner;

            case 20:
                return GameDifficulty.Advanced;

            case 30:
                return GameDifficulty.Expert;
            
            default:
                throw new Exception("There is no such GameDifficulty for Rows Count in GameData: " + rows);
        }
    }
}