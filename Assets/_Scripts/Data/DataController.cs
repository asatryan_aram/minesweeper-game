﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class DataController : MonoBehaviour, IController
{
    public static DataController Instance;

    private const string GameKey = "mineseeper";
    
    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
    }

    void Update()
    {
    }

    public void Init()
    {
    }

    public void Restart()
    {
    }

    public void LoadGame()
    {
        string serializedCells = PlayerPrefs.GetString(GameKey, String.Empty);

        if (serializedCells.Equals(String.Empty))
        {
            UIController.Instance.ShowLoadMessage(false);
            return;
        }

        List<List<Cell>> cells = JsonHelper.DeserializeCells(serializedCells);

        if (0 == cells.Count)
        {
            UIController.Instance.ShowLoadMessage(false);
            return;
        }
        
        BoardController.Instance.LoadGame(cells, GameData.GetGameDifficultyByRows(cells.Count));
        UIController.Instance.ShowLoadMessage(true);
    }

    public void SaveGame()
    {
        List<List<Cell>> cells = BoardController.Instance.GetCellsCurrentState();
        string serializedCells = JsonHelper.SerializeCells(cells);
        
        PlayerPrefs.SetString(GameKey, serializedCells);
        PlayerPrefs.Save();
        
        UIController.Instance.ShowSaveMessage(true);
    }

}