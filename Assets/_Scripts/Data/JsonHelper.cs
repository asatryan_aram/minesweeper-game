﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class JsonHelper {

    public static List<List<Cell>> DeserializeCells(string data)
    {
        string decodeData = Base64Decode(data);
        return JsonConvert.DeserializeObject<List<List<Cell>>>(decodeData) ?? new List<List<Cell>>();
    }
    
    public static string SerializeCells(List<List<Cell>> cells)
    {
        var serializeMessages = JsonConvert.SerializeObject(cells);
        return Base64Encode(serializeMessages);
    }

    private static string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }

    private static string Base64Decode(string base64EncodedData)
    {
        var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
        return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
    }
}
