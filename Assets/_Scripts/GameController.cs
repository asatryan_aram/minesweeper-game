﻿using UnityEngine;

public class GameController : MonoBehaviour, IController
{
    private static GameController _instance = null;
    public static GameController Instance { get { return _instance; } }
    
    void Awake()
    {
        if (_instance)
        {
            Destroy(this.gameObject);
            return;
        }
        _instance = this;
        
        DontDestroyOnLoad(this);
        this.transform.parent = null;
        Application.targetFrameRate = 30;
    }

    void Start()
    {
        Init();
    }

    void Update()
    {
    }

    public void Init()
    {
        UIController.Instance.Init();
        BoardController.Instance.Init();
        DataController.Instance.Init();
    }

    public void Restart()
    {
        UIController.Instance.Restart();
        BoardController.Instance.Restart();
        DataController.Instance.Restart();
    }
    
    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
    
    void OnApplicationQuit()
    {
        ClearInstances();
    }
    
    private void ClearInstances()
    {
        if (BoardController.Instance) { BoardController.Instance.StopAllCoroutines(); BoardController.Instance.CancelInvoke(); BoardController.Instance = null;}
        if (UIController.Instance) { UIController.Instance.StopAllCoroutines(); UIController.Instance.CancelInvoke(); UIController.Instance = null;}
        if (DataController.Instance) { DataController.Instance.StopAllCoroutines(); DataController.Instance.CancelInvoke(); DataController.Instance = null;}
    }
}