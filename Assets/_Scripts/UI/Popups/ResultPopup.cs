﻿using UnityEngine;
using UnityEngine.UI;

public class ResultPopup : Popup
{
    [SerializeField] private Text _textTitle;

    private const string TextWin = "YOU WON !!!";

    private const string TextFail = "YOU FAILED (";
    
   public void ButtonClosePressedHandler()
    {
        Hide();
    }
  
    public void ButtonRestartPressedHandler()
    {
        GameController.Instance.Restart();
        Hide();
    }

    public  void Show(bool isWin)
    {
        _textTitle.text = isWin ? TextWin : TextFail;
        Show();
    }
}