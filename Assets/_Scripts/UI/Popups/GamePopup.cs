﻿using UnityEngine;
using UnityEngine.UI;

public class GamePopup : Popup
{
    public void ButtonClosePressedHandler()
    {
        Hide();
    }

    public void ButtonChangeGameDifficultyToBeginnerHandler()
    {
        BoardController.Instance.ChangeGameDifficulty(GameDifficulty.Beginner);
        Hide();
    }
    
    public void ButtonChangeGameDifficultyToAdvancedHandler()
    {
        BoardController.Instance.ChangeGameDifficulty(GameDifficulty.Advanced);
        Hide();
    }
    
    public void ButtonChangeGameDifficultyToExpertHandler()
    {
        BoardController.Instance.ChangeGameDifficulty(GameDifficulty.Expert);
        Hide();
    }

    public void ButtonRestartPressedHandler()
    {
        GameController.Instance.Restart();
        Hide();
    }

    public void ButtonLoadGamePressedHandler()
    {
        DataController.Instance.LoadGame();
        Hide();
    }
    
    public void ButtonSaveGamePressedHandler()
    {
        DataController.Instance.SaveGame();
        Hide();
    }
}