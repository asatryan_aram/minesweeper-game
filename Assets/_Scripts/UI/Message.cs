﻿using UnityEngine;
using UnityEngine.UI;

public class Message : MonoBehaviour
{
    [SerializeField] private Text _textMessage;

    private const int HideTime = 2;

    private const string SaveSuccess = "Game Saved";
    
    private const string LoadSuccess = "Game Loaded";
    
    private const string LoadFailed = "There Is Nothing To Loaded";

    void Start()
    {
    }

    void Update()
    {
    }

    public void ShowSaveMessage(bool isSuccess)
    {
        _textMessage.text = SaveSuccess;
        ShowMessage();
    }
    
    public void ShowLoadMessage(bool isSuccess)
    {
        _textMessage.text = isSuccess ? LoadSuccess : LoadFailed;
        ShowMessage();
    }

    private void ShowMessage()
    {
        CancelInvoke();
        _textMessage.gameObject.SetActive(true);
        Invoke("Hide", HideTime);
    }

    private void Hide()
    {
        _textMessage.gameObject.SetActive(false);
    }
}