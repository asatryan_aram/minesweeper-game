﻿using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class UIController : MonoBehaviour, IController
{
    public static UIController Instance;
    
    private Popup _popupGame;
    private ResultPopup _popupResult;
    private Message _message;
    
    void Awake()
    {
        Instance = this;
        
        _popupGame = FindObjectOfType<GamePopup>();
        _popupResult = FindObjectOfType<ResultPopup>();
        _message = FindObjectOfType<Message>();
    }

    void Start()
    {
    }

    void Update()
    {
    }

    public void Init()
    {
    }

    public void Restart()
    {
    }

    public void ButtonMenuPressedHandler()
    {
        ShowGamePopup();
    }

    public void ShowGamePopup()
    {
        _popupGame.Show();
    }

    public void ShowResultPopup(bool isWin)
    {
        _popupResult.Show(isWin);
    }

    public void ShowSaveMessage(bool isSuccess)
    {
        _message.ShowSaveMessage(isSuccess);
    }
    
    public void ShowLoadMessage(bool isSuccess)
    {
        _message.ShowLoadMessage(isSuccess);
    }
}