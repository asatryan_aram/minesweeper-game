﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BoardCell : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private GameObject _unopened;
    [SerializeField] private GameObject _flag;
    [SerializeField] private GameObject _bomb;
    [SerializeField] private GameObject _explode;
    [SerializeField] private Text _textCell;

    public int Col { get; private set; }
    public int Row { get; private set; }

    private bool _isFlagged;
    public bool IsFlagged { get { return _isFlagged; } }
    
    private bool _isOpened;
    public bool IsOpened { get { return _isOpened; } }

    private int _number;
    public int Number { get { return _number; } }

    private CellState _cellState;
    public CellState CurrentCellState { get { return _cellState; } }

    public void Init(int number, int row, int col, int rows, int cols, int cellSize, bool isFlagged, bool isOpened)
    {
        Init(number, row, col, rows, cols, cellSize);

        _isFlagged = isFlagged;
        _isOpened = isOpened;

        _flag.SetActive(_isFlagged && !_isOpened);
        _unopened.SetActive(!_isOpened);
    }
    
    public void Init(int number, int row, int col, int rows, int cols, int cellSize)
    {
        transform.localPosition = BoardCellHelper.GetPosition(row, col, rows, cols, cellSize);
        Row = row;
        Col = col;
        _number = number;
        InitialState();
    }

    private CellState GetState()
    {
        if (_number == -1) return CellState.Bomb;

        if (_number == 0) return CellState.Empty;

        return CellState.Number;
    }

    public void Restart()
    {
        InitialState();
    }

    private void InitialState()
    {
        _textCell.text = String.Empty;
        _isFlagged = false;
        _isOpened = false;
        _flag.SetActive(false);
        _unopened.SetActive(true);
        _bomb.SetActive(false);
        _explode.SetActive(false);
        _cellState = GetState();
        ShowState();
    }

    private void ShowState()
    {
        if (_cellState == CellState.Bomb)
        {
            _textCell.text = String.Empty;
            _bomb.SetActive(true);
        }
        else if (_cellState == CellState.Number)
        {
            _textCell.text = _number.ToString();
            _bomb.SetActive(false);
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (_isOpened) return;

        if (eventData.button == PointerEventData.InputButton.Right)
        {
            ChangeFlagState();
        }
        else if (eventData.button == PointerEventData.InputButton.Left)
        {
            OpenCell();
        }
    }

    private void OpenCell()
    {
        if (_isFlagged) return;

        _unopened.SetActive(false);
        _isOpened = true;

        if (_cellState == CellState.Bomb)
        {
            _explode.SetActive(true);
        }

        BoardController.Instance.CellClicked(this);
    }

    public void Show()
    {
        _isOpened = true;
        _unopened.SetActive(false);
        _flag.SetActive(false);
    }

    private void ChangeFlagState()
    {
        _isFlagged = !_isFlagged;
        _flag.SetActive(_isFlagged);
    }
}