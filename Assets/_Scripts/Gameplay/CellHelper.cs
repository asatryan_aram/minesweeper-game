﻿using System.Collections.Generic;
using UnityEngine;

public class CellHelper
{
    private List<List<Cell>> _cells;
    public List<List<Cell>> Cells { get { return _cells; } set { _cells = value; } }

    private int _rows;
    private int _cols;
    private int _minesCount;


    public CellHelper()
    {
        _cells = new List<List<Cell>>();
    }

    public void Generate(int rows, int cols, int minesCount)
    {
        _rows = rows;
        _cols = cols;
        _minesCount = minesCount;
        Generate();
    }

    public void Generate()
    {
        CreateEmptyCells();
        GenerateRandomPositionForMine();
    }

    private void CreateEmptyCells()
    {
        _cells.Clear();
        _cells = new List<List<Cell>>();

        for (int row = 0; row < _rows; ++row)
        {
            List<Cell> cells = new List<Cell>();
            for (int col = 0; col < _cols; ++col)
            {
                Cell cell = new Cell {Row = row, Col = col};
                cells.Add(cell);
            }

            _cells.Add(cells);
        }
    }

    private void GenerateRandomPositionForMine()
    {
        int count = 0;
        while (count < _minesCount)
        {
            int randRow = Random.Range(0, _rows);
            int randCol = Random.Range(0, _cols);

            Cell cell = _cells[randRow][randCol];

            if (-1 == cell.Number)
            {
                continue;
            }

            PutMines(randRow, randCol);
            ++count;
        }
    }

    private Cell GetCellAt(int row, int col)
    {
        if (row < 0 || row >= _rows || col < 0 || col >= _cols)
        {
            return null;
        }

        return _cells[row][col];
    }

    private void PutMines(int row, int col)
    {
        _cells[row][col].Number = -1;

        Cell cell = GetCellAt(row - 1, col);
        if (null != cell && -1 != cell.Number)
        {
            cell.Number++;
        }

        cell = GetCellAt(row + 1, col);
        if (null != cell && -1 != cell.Number)
        {
            cell.Number++;
        }

        cell = GetCellAt(row, col - 1);
        if (null != cell && -1 != cell.Number)
        {
            cell.Number++;
        }

        cell = GetCellAt(row, col + 1);
        if (null != cell && -1 != cell.Number)
        {
            cell.Number++;
        }

        cell = GetCellAt(row - 1, col - 1);
        if (null != cell && -1 != cell.Number)
        {
            cell.Number++;
        }

        cell = GetCellAt(row - 1, col + 1);
        if (null != cell && -1 != cell.Number)
        {
            cell.Number++;
        }

        cell = GetCellAt(row + 1, col - 1);
        if (null != cell && -1 != cell.Number)
        {
            cell.Number++;
        }

        cell = GetCellAt(row + 1, col + 1);
        if (null != cell && -1 != cell.Number)
        {
            cell.Number++;
        }
    }
}