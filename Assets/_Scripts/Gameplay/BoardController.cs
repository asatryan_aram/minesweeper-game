﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BoardController : MonoBehaviour, IController
{
    [SerializeField] private Transform _boardContainer;

    [SerializeField] private GameObject _cellPrefab;

    [SerializeField] private GameDifficulty _gameDifficulty;

    public static BoardController Instance;

    private int _cellSize;

    private int _boardContainerWidth;

    private List<List<BoardCell>> _cells = new List<List<BoardCell>>();

    private BoardData _boardData;

    private CellHelper _cellHelper;


    void Awake()
    {
        Instance = this;
    }

    public void Init()
    {
        _boardContainerWidth = (int) _boardContainer.GetComponent<RectTransform>().sizeDelta.x;
        _cellHelper = new CellHelper();
        ChangeGameDifficulty(_gameDifficulty);
    }

    public void Restart()
    {
        _cellHelper.Generate();
        InitCellsWithData();
    }

    public void ChangeGameDifficulty(GameDifficulty gameDifficulty)
    {
        _gameDifficulty = gameDifficulty;
        _boardData = GameData.GetBoardData(_gameDifficulty);
        ClearBoard();
        InitParams();
        InstantiatePrefabs();
        InitCellsWithData();
    }

    public void CellClicked(BoardCell cell)
    {
        if (cell.CurrentCellState == CellState.Bomb)
        {
            UIController.Instance.ShowResultPopup(false);
            return;
        }

        if (cell.CurrentCellState == CellState.Empty)
        {
            OpenEmptyNeighboursCell(cell);
        }

        if (IsAllCellsOpen())
        {
            UIController.Instance.ShowResultPopup(true);
        }
    }

    private bool IsAllCellsOpen()
    {
        int openCellsCount = 0;

        for (int row = 0, countRow = _cells.Count; row < countRow; ++row)
        {
            List<BoardCell> cells = _cells[row];
            for (int col = 0, countCol = cells.Count; col < countCol; ++col)
            {
                if (_cells[row][col].IsOpened)
                {
                    ++openCellsCount;
                }
            }
        }

        return openCellsCount >= (_boardData.Rows * _boardData.Colls - _boardData.Mines);
    }

    private void OpenEmptyNeighboursCell(BoardCell cell)
    {
        List<BoardCell> cells = GetMatches(cell.Row, cell.Col);
        for (int i = 0, count = cells.Count; i < count; ++i)
        {
            cells[i].Show();
        }
    }

    private void InitParams()
    {
        _cellSize = _boardContainerWidth / _boardData.Rows;
        _cellPrefab.GetComponent<RectTransform>().sizeDelta = new Vector2(_cellSize, _cellSize);
        _cellHelper.Generate(_boardData.Rows, _boardData.Colls, _boardData.Mines);
    }

    private void InstantiatePrefabs()
    {
        for (int row = 0, rowCount = _boardData.Rows; row < rowCount; ++row)
        {
            List<BoardCell> rowCells = new List<BoardCell>();
            for (int col = 0, colCount = _boardData.Colls; col < colCount; ++col)
            {
                GameObject prefab = Instantiate(_cellPrefab, _boardContainer, false);
                BoardCell boardCell = prefab.GetComponent<BoardCell>();
                rowCells.Add(boardCell);
            }

            _cells.Add(rowCells);
        }
    }

    private void InitCellsWithData()
    {
        for (int row = 0, countRow = _cells.Count; row < countRow; ++row)
        {
            List<BoardCell> cells = _cells[row];
            for (int col = 0, countCol = cells.Count; col < countCol; ++col)
            {
                BoardCell cell = _cells[row][col];
                cell.Init(_cellHelper.Cells[row][col].Number, row, col, _boardData.Rows, _boardData.Colls, _cellSize);
            }
        }
    }

    private void ClearBoard()
    {
        for (int row = 0, countRow = _cells.Count; row < countRow; ++row)
        {
            List<BoardCell> cells = _cells[row];
            for (int col = 0, countCol = cells.Count; col < countCol; ++col)
            {
                BoardCell boardCell = _cells[row][col];
                boardCell.Hide();
                Destroy(boardCell.gameObject);
            }
        }

        _cells.Clear();
    }

    public List<BoardCell> GetMatches(int row, int col)
    {
        List<BoardCell> boardCells = GetNeighbours(row, col);
        int startIndex = 0;
        while (true)
        {
            int prevCount = boardCells.Count;
            bool newCubeAdded = false;
            for (int i = startIndex, count = boardCells.Count; i < count; ++i)
            {
                BoardCell cell = boardCells[i];

                if (0 != cell.Number)
                {
                    continue;
                }

                List<BoardCell> neighbors = GetNeighbours(cell.Row, cell.Col);

                for (int j = 0; j < neighbors.Count; ++j)
                {
                    BoardCell neighbor = neighbors[j];
                    
                    if (boardCells.IndexOf(neighbor) == -1)
                    {
                        boardCells.Add(neighbor);
                        newCubeAdded = true;
                    }
                }
            }

            startIndex = prevCount;

            if (newCubeAdded == false)
            {
                break;
            }
        }

        return boardCells;
    }

    private List<BoardCell> GetNeighbours(int row, int col)
    {
        List<BoardCell> cells = new List<BoardCell>();

        BoardCell cell = GetCellAt(row - 1, col);
        if (null != cell && 0 <= cell.Number)
        {
            cells.Add(cell);
        }

        cell = GetCellAt(row + 1, col);
        if (null != cell && 0 <= cell.Number)
        {
            cells.Add(cell);
        }

        cell = GetCellAt(row, col - 1);
        if (null != cell && 0 <= cell.Number)
        {
            cells.Add(cell);
        }

        cell = GetCellAt(row, col + 1);
        if (null != cell && 0 <= cell.Number)
        {
            cells.Add(cell);
        }

        return cells;
    }

    private BoardCell GetCellAt(int row, int col)
    {
        if (row < 0 || row >= _cells.Count || col < 0 || col >= _cells.Count)
        {
            return null;
        }

        return _cells[row][col];
    }

    public List<List<Cell>> GetCellsCurrentState()
    {
        List<List<Cell>> cells = new List<List<Cell>>();
        
        for (int row = 0, countRow = _cells.Count; row < countRow; ++row)
        {
            List<BoardCell> boardCells = _cells[row];
            List<Cell> rowCells = new List<Cell>();
            for (int col = 0, countCol = boardCells.Count; col < countCol; ++col)
            {
                BoardCell boardCell = _cells[row][col];
                Cell cell = new Cell {Row = boardCell.Row, Col = boardCell.Col, Number = boardCell.Number, IsFlagged = boardCell.IsFlagged, IsOpened = boardCell.IsOpened};
                rowCells.Add(cell);
            }
            cells.Add(rowCells);
        }
        
        return cells;
    }


    public void LoadGame(List<List<Cell>> cells, GameDifficulty gameDifficulty)
    {
        _gameDifficulty = gameDifficulty;
        _boardData = GameData.GetBoardData(_gameDifficulty);
        ClearBoard();
        InitParams();
        _cellHelper.Cells = cells;
        InstantiatePrefabs();
        InitCellsWithSavedData();
    }
    
    private void InitCellsWithSavedData()
    {
        for (int row = 0, countRow = _cells.Count; row < countRow; ++row)
        {
            List<BoardCell> cells = _cells[row];
            for (int col = 0, countCol = cells.Count; col < countCol; ++col)
            {
                BoardCell cell = _cells[row][col];
                Cell cellData = _cellHelper.Cells[row][col];
                cell.Init(cellData.Number, row, col, _boardData.Rows, _boardData.Colls, _cellSize, cellData.IsFlagged, cellData.IsOpened);
            }
        }
    }
}