﻿using UnityEngine;

public class BoardCellHelper
{
    public static Vector2 GetPosition(int row, int col, int rows, int cols, int cellSize)
    {
        int posX = cellSize * row;
        int posY = cellSize * -col;
        return new Vector2(posX, posY);
    }
    
    
}